# TD1
# Nathan Houzet M2GL G2

# Utilisation d’OpenStack

**Le lien du GIT [ici](https://gitlab.univ-lille.fr/nathan.houzet.etu/bdda-tp1).**

# Pour s'y connnecter via ssh et vpn: 
`ssh ubuntu@172.28.100.65`

Utilisateur disponible sur la VM :
Lambda : remote  (mdp remote)  
Charles : chat (mpd 1234)

[comment]: <> (utile pour moi, mémo)
[comment]: <> (depuis remote pour aller dans psql, taper psql)
[comment]: <> (ssh houzet@b10p18.fil.univ-lille1.fr)

Pour se connecter à la base micoblog à distance via terminal: 

Utilisateur remote :`psql -h 172.28.100.65 -U remote --dbname microblog` // (pass remote)

Utilisateur cha :`psql -h 172.28.100.65 -U cha --dbname microblog` // (pass ILovePostgreSQL)




# Une application de micro-blogging
## Réponses aux questions :

### 7. Lisez le fichier `microblog.sql` et expliquez brièvement les différentes fonctionnalité de cette base de données.

List of relations :

 Schema |      Name      | Type  | Owner  
--------|----------------|-------|--------
 public | followers      | table | remote
 public | messages_store | table | remote
 public | user_store     | table | remote

followers|
---------|
user_source | 
user_target |


**users_store** |      
--------|
 user_id |
name |
password |


messages_store|
--------------|
 message_id | 
 content | 
 published_date |
 replies_to |
 user_id |

Nous avons 3 tables, 
<li> Une table stockant les users ( id, nom, password)
<li>Une table stokant les messages (id, content, date de publication, réponse à , et posteur )
<li> Une table suivant les following ( source et target)

Le concept de microblog est donc repris car nous avons une relation entre des 
utilisateurs, un système de chat et des followings.

### 8. Est-ce qu’il est possible pour l’utilisateur common_user:
<li>  de récupérer la totalité des messages publiés ?
	<ul>	
		<li> le common_user n'as pas accès à la table messages_store et ne peux donc pas voir la totalité des message via un ``` select * from messages-store```.
		<li> il peux cependant avoir accès à la view messages qui correspond au 30 derniers messages ajoutés.
	</ul>
<li>  de récupérer la liste des utilisateurs inscrits ?
	<ul>	
		<li> le common_user n'as pas accès à la table users_store et ne peux donc pas voir la liste des utilisateurs inscrits.
	</ul>
<li>  de récupérer le mot de passe d’un utilisateur  inscrit ?
	<ul>	
		<li> le common_user n'as pas accès à la table users_store et ne peux donc pas voir la liste des mots de passe des utilisateurs.
	</ul>

### Est-il possible pour l’utilisateur possédant la base de données

<li>  de récupérer la totalité des messages publiés
<ul>	
		<li> oui
	</ul>
<li>  de récupérer la liste des utilisateurs inscrits
<ul>	
		<li> oui via un select sur la table user_store
	</ul>
<li>  de récupérer le mot de passe d’un utilisateur inscrit
<ul>	
		<li> non car les mots de passes sont hachés avant l'ajout dans la table
	</ul>

###  10.  A quoi sert la ligne ?
```
CLUSTER messages_store USING idx_pub_date;
```
Cette ligne sert à indexer la table  messages_store  gràce à l'index idx_pub_date ( date de publication des messages).


# Génération de post sur le microblog : 

Avec l'archive jar : 2 commandes sont possible pour aider à simuler les posts :

La commande `bdda-tp1$ java -jar out/artifacts/postgresqlJava_jar/postgresqlJava.
  jar generate 10` va générer 10 utilisateurs dans la DB et stocker leur nom et mdp 
  dans un fichier .txt (le mot de passse etant leur nom)  :

```bash   
  bdda-tp1$ java -jar out/artifacts/postgresqlJava_jar/postgresqlJava.
  jar generate 10
  Connection succesfull
  df340298-2156-11ec-8d62-0d507adc61fc
  df340299-2156-11ec-8d62-0d507adc61fc
  df34029a-2156-11ec-8d62-0d507adc61fc
  df34029b-2156-11ec-8d62-0d507adc61fc
  df34029c-2156-11ec-8d62-0d507adc61fc
  df34029d-2156-11ec-8d62-0d507adc61fc
  df34029e-2156-11ec-8d62-0d507adc61fc
  df34029f-2156-11ec-8d62-0d507adc61fc
  df3402a0-2156-11ec-8d62-0d507adc61fc
  df3402a1-2156-11ec-8d62-0d507adc61fc
  Names are stored in the file :.../Documents/M2S1/bdd/bdda-tp1/name.txt
```

La commande ```bdda-tp1$ java -jar 
out/artifacts/postgresqlJava_jar/postgresqlJava.jar simule 0 5``` va simuler des post 
toutes les secondes pendant 5 secondes de l'utilisateur 0. ( le premier utilisateur de 
la liste des utilisateurs crées.)

Essais réalisés via le [script](100users.sh) adapté au nombre n d'utilisateur postant 
toutes les secondes:

SUR Simulation faite sur un poste du M5 : 

<ul>
<li> TPS MAX de 87 pour 50 utilisateurs en //.
<li> TPS MAX de 160  pour 100 utilisateurs en //.
<li> TPS MAX de 137 pour 150 utlisateurs en //.
<li> TPS MAX de 138 pour 200 utilisateurs en //. A noter + de 150 requete en attentes
<li> TPS MAX de 120 pour 300 utilisateurs en // . En attentes au alentours de 100 
reuqtes + Erreur ci dessous après quelques secondes de lancement.
<li> 500 users -> message d'erreurs `PSQLException: FATAL: sorry, too many clients 
already` + message d'erreur pg-activity :  `pg_activity: FATAL: the database system is in recovery mode  the database system is in recovery mode`
</ul>

On peut voir que plus les utilisateurs envoient des post sur le microblog, plus le nombre 
de requetes traités par seconde par la base de données est élévé. J'ai atteint un max 
de 138 requetes par secondes pour 200 utilisateurs blogant en meme temps toutes les 
secondes.
A noter que au même moment, plus de 150 requêtes étaient en attente à chaque seconde 
par la database.
Au dela de 300 utilisateurs la database se met à craquer, en refusant les requetes ( 
cf PSQLEcxeption de ci-dessus).  
Egalement le surveillant d'activité de la base recoit une erreur indiquant que la base de 
données n'est plus accessible.

Je pense que cela doit etre due à un manque de mémoire de la VM Hébergeant la base de 
donnée.

En effet la VM configurée lors de cette simulation comportait 512 MO de mémoire vive.

La performance de la VM, qui de mon point de vu, est déjà très honorable pour une 
machine hébergeant un os (assez léger) et une base de données se fesant réquéter par 300 
utlisateurs chaque secondes.

Je n'ai pas pu étendre mon benchmarking à une autre VM avec une plus grande capacité de 
mémoire car cela demande de reconfigurer une autre VM et une autre base de donnée 
aussi.
Cependant, on peut facilement se douter qu'avec une plus grande capcité de mémoire 
disponible, la base de donnée pourra soutenir plus de requetes à un rythme plus soutenu.

**Vous trouverez le Main permettant de créer les users et les envois de message [ici](MicroblogJava/src/main/java/postgresqlJava/App.java).**

**L'api utilisé pour se connecter à la db [ici](MicroblogJava/src/main/java/postgresqlJava/Microblog.java)**

**Le lien du GIT [ici](https://gitlab.univ-lille.fr/nathan.houzet.etu/bdda-tp1).**



