package postgresqlJava;


import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * App
 *
 */
public class App 
{
    /**
     * 2 mode / argument
     *
     * "generate n " will generate n users in the microblog
     */
    public static void main( String[] args ) throws SQLException, FileNotFoundException, InterruptedException {
        Microblog microblog = new Microblog("172.28.100.65");

        if (args[0].equals("generate")) {
            generateAndAddUser(Integer.parseInt(args[1]), microblog);
        }
        if (args[0].equals("simule")){
            liveOfOne(Integer.parseInt(args[1]),"name.txt",microblog,Integer.parseInt(args[2]));
        }

    }

    /**
     * Method witch create and add person on the database.
     * Il also produce a txt file witch contains the names of the person created.
     * The password of the person in the data base is his name.
     * @param n the number of people created
     * @param mb the Microblog to add the persons.
     */
    public static void generateAndAddUser(int n, Microblog mb){

        String result = null;
        try {
            FileWriter file = new FileWriter("name.txt");

            for (int i = 0; i<n; i++){
                Runtime r = Runtime.getRuntime();
                Process p = r.exec("rig");
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String inputLine;

                result = in.readLine();
                if (result != null) {
                    mb.createUser(result,result);
                    file.write(result+"\n");


                }
                in.close();


            }
            System.out.println("Names are stored in the file :" + System.getProperty("user.dir")+"/name.txt" );
            file.close();


        } catch (IOException | SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Method that made a user of the database alive. He will post a message every seconds
     * @param id the id of the user
     * @param names the list (txt file) of the data bases users
     * @param mb the microblog data base object
     * @param time the number of second you want the blogger to be active
     * @throws FileNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     */
    public static void liveOfOne(int id, String names, Microblog mb, int time) throws FileNotFoundException, SQLException, InterruptedException {
        ArrayList<String> users = new ArrayList<String>();

        BufferedReader br = new BufferedReader(new FileReader(names));
        try {
            String user = br.readLine();
            while (user != null) {
                users.add(user);
                user = br.readLine();
            }


        }catch (IOException e) {
            System.out.println(e);
        }

        //TODO l'utilisateur courant sera le n° id de la liste de personnes.
        // Simuler sa vie : le faire poster toute les 1 secs.

        long t= System.currentTimeMillis();
        long end = t+time* 1000L;
        while(System.currentTimeMillis() < end) {
            // do something
            mb.publish_post(users.get(id),users.get(id),text(16));
            // pause to avoid churning
            Thread.sleep( 1000 );

        }
        return;

    }

    /**
     * Generate random text of lenght size
     * @param Askedlength length of the text generated
     * @return
     */
    public static String text(int Askedlength){

        // create a string of all characters
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = Askedlength;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphabet.length());

            // get character specified by index
            // from the string
            char randomChar = alphabet.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
        return randomString;

    }

}
