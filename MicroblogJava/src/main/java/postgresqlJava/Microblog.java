package postgresqlJava;

import java.sql.*;
import java.util.UUID;

public class Microblog {

    public Connection c;

    public Microblog(String ip){
        try{
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://"+ip+":5432/microblog","common_user","");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Connection succesfull");
    }


    /**
     * Publish a post
     * @param user the name of the user
     * @param password the password of the user
     * @param content the content
     * @throws SQLException on SQL Exception
     */
    public void publish_post(String user,String password,String content) throws SQLException {
        publish_post(user,password,content,null);
    }


    /**
     * Publish a post
     * @param user the username of the user
     * @param password the password of the user
     * @param content the content of the message
     * @param uuid the uuid of the user that the post reply to (nullable)
     * @throws SQLException on SQLException
     */
    public void publish_post(String user, String password, String content, UUID uuid) throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement("SELECT insert_message(?,?,?,?)");
        preparedStatement.setString(1,user);
        preparedStatement.setString(2,password);
        preparedStatement.setString(3,content);
        preparedStatement.setObject(4,uuid);
        if(! (preparedStatement.execute())){
            System.out.println("Error While insert Message");
        }else{
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                System.out.println(resultSet.getObject(1));
            }
            System.out.println("Message Insert");
        }
    }

    /**
     * Create a user
     * @param username the name of the user
     * @param password the password of the user
     * @throws SQLException on SQLException
     */
    public void createUser(String username, String password) throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement("SELECT create_user(?,?)");
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        if(! (preparedStatement.execute())){
            System.out.println("Error While creating user");
        }else{
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()) {
                System.out.println(resultSet.getObject(1));
            }
        }
    }

    /**
     * Follow a user
     * @param userame the name of the user
     * @param password the password of the user
     * @param uuid the uuid of the user to follow
     * @throws SQLException on SQLException
     */
    public void follow(String userame,String password,UUID uuid) throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement("SELECT follow(?,?,?)");
        preparedStatement.setString(1,userame);
        preparedStatement.setString(2,password);
        preparedStatement.setObject(3,uuid);
        if(! (preparedStatement.execute())){
            System.out.println("Error While creating user");
        }else{
            System.out.println("Follow succes");
        }
    }

    /**
     * Get the feed of user
     * @param username the username
     * @param password the password
     * @throws SQLException on SQLException
     */
    public void get_feed(String username,String password) throws SQLException {
        get_feed(username,password,50);
    }

    /**
     * Get the feed of user
     * @param username the username
     * @param password the password
     * @param limit on SQLException
     * @throws SQLException
     */
    public void get_feed(String username,String password, int limit) throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement("SELECT feed(?,?) LIMIT ?");
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        preparedStatement.setInt(3,limit);
        if(! (preparedStatement.execute())){
            System.out.println("Error While creating user");
        }else{
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()) {
                System.out.println("Feed row");
                System.out.println(resultSet.getObject(1));
                System.out.println("\n");
            }
        }
    }

    /**
     * Get view Messages
     */
    public void get_messages() throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement("SELECT * from messages");
        if(! (preparedStatement.execute())){
            System.out.println("Error While creating user");
        }else {
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                System.out.println("Message Row");
                System.out.println(resultSet.getObject(1));
                System.out.print(resultSet.getString(2));
                System.out.println(resultSet.getDate(3));
                System.out.println(resultSet.getObject(4));
                System.out.println(resultSet.getString(5));
                System.out.println(resultSet.getObject(6));
                System.out.println("\n");
            }
        }

    }

}
